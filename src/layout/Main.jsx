import { useMemo, useState } from "react";
import Pagination from "../components/Pagination";
import SearchForm from "../components/SearchForm";
import Table from "../components/Table";
import useSearch from "../hooks/useSearch";
import "../styles.scss";

let pageSize = 10;

const Main = () => {

  const [query, setQuery] = useState("");

  const [currentPage, setCurrentPage] = useState(1);

  const { isLoading, error, tableData } = useSearch(query, currentPage);

  const currentTableData = useMemo(() => {
    const firstPageIndex = (currentPage - 1) * pageSize;
    const lastPageIndex = firstPageIndex + pageSize;
    return tableData.slice(firstPageIndex, lastPageIndex);
  }, [currentPage, tableData]);

  const handleChange = (e) => {
    const { value } = e.target;
    setQuery(value);
    setCurrentPage(1);
  };

  return (
    // Empty Fragment
      <>
````````<SearchForm query={query} handleChange={handleChange} />
        <Table
          isLoading={isLoading}
          error={error}
          tableData={currentTableData}
        />

        <div className="d-flex justify-content-center">
          <Pagination
            className="pagination-bar"
            currentPage={currentPage}
            totalCount={tableData.length}
            pageSize={pageSize}
            onPageChange={(page) => setCurrentPage(page)}
          />
        </div>
      </>

  );
};

export default Main;
