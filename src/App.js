
import "./styles.scss";
import { Routes, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import Main from "./layout/Main";


const App = () => {
  
  return (
    <div className="App">
      <Navbar />
      <div className="container">
        <Routes>
            <Route path={"/"} element={<Main />}></Route>
        </Routes>
      </div>
    </div>
  );
};

export default App;
