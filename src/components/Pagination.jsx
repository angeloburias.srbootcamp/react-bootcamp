import { usePagination, DOTS } from "../hooks/usePagination";
import classnames from "classnames";

const Pagination = ({
  siblingCount = 1,
  currentPage = 1,
  pageSize,
  totalCount,
  onPageChange,
  className,
}) => {
  const paginationRange = usePagination({
    siblingCount,
    currentPage,
    pageSize,
    totalCount,
  });

  if (currentPage === 0 || paginationRange.length < 2) return null;

  const handleNext = () => {
    onPageChange(currentPage + 1);
  };

  const handlePrevious = () => {
    onPageChange(currentPage - 1);
  };

  let lastPage = paginationRange[paginationRange.length - 1];

  return (
    <ul
      className={classnames("pagination-container", { [className]: className })}
    >
      {/* START: Left Nagivation Arrow  */}
      <li
        className={classnames("pagination-item", {
          disabled: currentPage === 1,
        })}
        onClick={handlePrevious}
      >
        <div className="arrow left"></div>
      </li>
      {/* END: Left Navigation Arrow  */}

      {/* START: Render our Page Numbers  */}
      {paginationRange?.map((pageNumber, idx) => {
        if (pageNumber === DOTS)
          return (
            <li key={idx} className="pagination-item dots">
              &#8230;
            </li>
          );

        // Render our Page Pills

        return (
          <li
            key={idx}
            className={classnames("pagination-item", {
              selected: pageNumber === currentPage,
            })}
            onClick={() => onPageChange(pageNumber)}
          >
            {pageNumber}
          </li>
        );
      })}
      {/* END: Render our Page numbers  */}

      {/* START: Right navigation Arrow  */}
      <li
        className={classnames("pagination-item", {
          disabled: currentPage === lastPage,
        })}
        onClick={handleNext}
      >
        <div className="arrow right"></div>
      </li>
      {/* END: Rigt Navigation Arrow  */}
    </ul>
  );
};

export default Pagination;
