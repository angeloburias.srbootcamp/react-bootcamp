const SearchForm = ({ query, handleChange }) => {
  return (
    <div className="search-wrapper mt-3 mb-3">
      <div className="form-group">
        <input
          value={query}
          onChange={handleChange}
          type="text"
          className="form-control"
          placeholder="Search ..."
        />
      </div>
    </div>
  );
};

export default SearchForm;
