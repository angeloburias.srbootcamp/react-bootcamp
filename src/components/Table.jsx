const Table = ({ isLoading, error, tableData }) => {
  return (
    <div className="table-wrapper mb-3">
      <table className="table table-bordered table-responsive">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Title</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          {[].concat(tableData)?.map((item, index) => (
            <tr key={index}>
              <td>{item.id}</td>
              <td>{item.title}</td>
              <td className="gap-2 d-flex justify-content-center">
                <button className="btn btn-dark btn-sm">Delete</button>
                <button className="btn btn-primary btn-sm">Update</button>
              </td>
            </tr>
          ))}
          {isLoading && (
            <tr>
              <td colSpan={3}>LOADING ...</td>
            </tr>
          )}
          {!tableData.length && (
            <tr>
              <td colSpan={3}>NO RESULTS FOUND!</td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
