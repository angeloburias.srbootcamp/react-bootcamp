import axios from 'axios';
import { useEffect, useState } from 'react';

const useSearch = (query, pageNumber) => {
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(false);
  const [tableData, setTableData] = useState([]);

  useEffect(() => {
    setIsLoading(true);
    setError(false);

    let cancel;

    const response = axios({
      method: "GET",
      url: "https://jsonplaceholder.typicode.com/posts",
      params: { title_like: query },
      cancelToken: new axios.CancelToken((c) => (cancel = c)),
    });

    response
      .then((response) => {
        setTableData(response.data);
        setIsLoading(false);
      })
      .catch((err) => {
        if (axios.isCancel(err)) return;
        setError(true);
      });

    return () => cancel();
  }, [query, pageNumber]);

  return {
    isLoading,
    error,
    tableData
  }
}
 
export default useSearch;