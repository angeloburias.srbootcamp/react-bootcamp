import { useMemo } from "react";

export const DOTS = "...";

const range = (start, end) => {
  let length = end - start + 1;
  return Array.from({ length }, (_, idx) => idx + start);
}

export const usePagination = ({ totalCount, pageSize, currentPage = 1, siblingCount = 1 }) => {
 
  const paginationRange = useMemo(() => {
    const totalPage = Math.ceil(totalCount / pageSize); // 100 / 10 = 10

    const totalPageNumbers = siblingCount + 5; // 6

     //  < 1, 2, 3, 4, 5, ... 10 >
    if (totalPageNumbers >= totalPage) { // 10,10
      return range(1, totalPage);
    }

    // 1
    const leftIndex = Math.max(currentPage - siblingCount, 1);

    // 1, (2, 3, 4, 5, 6) siblings
    const rightIndex = Math.min(currentPage + siblingCount, totalPage);

    // 1 ... 4 5 ... 99 
    const showLeftDots = leftIndex > 2; // true

    // 3
    const showrightDots = rightIndex < totalPage - 2;

    const startPage = 1;
    const lastPage = totalCount - 1; // 99

    // 1, 3, 4, 5 ... 10
    if (!showLeftDots && showrightDots) {
      let leftItemCount = 3 + 2 * siblingCount;
      let leftRange = range(1, leftItemCount);

      return [...leftRange, DOTS, totalPage];
    }

    if (showLeftDots && !showrightDots) {
      let rightItemCount = 3 + 2 * siblingCount;
      let rightRange = range(totalPage - rightItemCount + 1, totalPage);

      return [startPage, DOTS, ...rightRange];
    }

    if (showLeftDots && showrightDots) {
      let middleRange = range(leftIndex, rightIndex);
      return [startPage, DOTS, ...middleRange, DOTS, lastPage];
    }
  }, [currentPage, pageSize, totalCount, siblingCount]);

  return paginationRange;
}